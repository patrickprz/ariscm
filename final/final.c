// LCD module connections
sbit LCD_RS at GPIOD_ODR.B2;
sbit LCD_EN at GPIOD_ODR.B3;
sbit LCD_D4 at GPIOD_ODR.B4;
sbit LCD_D5 at GPIOD_ODR.B5;
sbit LCD_D6 at GPIOD_ODR.B6;
sbit LCD_D7 at GPIOD_ODR.B7;
// End LCD module connections
char txt[4], tempo;
long aux;
//char hour, minute, second;
void rtc_init();

void get_hour();
void get_data();

void set_rtc();

void set_year_rtc();
void set_month_rtc();
void set_day_rtc();
void set_minute_rtc();
void set_hour_rtc();

void ldr();
void pwm();

int second = 0;
int minute = 0;
int hour = 0;
int day = 0;
int month = 0;
int year = 0;
int day_month = 0;
int result = 0;
int aux_ldr = 0;
int cont_ldr = 0;
char result_txt[4];
unsigned int current_duty, old_duty, pwm_period;
void main(){
    gpio_config(&gpioe_base, _gpio_pinmask_2 | _gpio_pinmask_3 | _gpio_pinmask_4 | _gpio_pinmask_5 | _gpio_pinmask_6,
                             _gpio_cfg_mode_input | _gpio_cfg_pull_up);
    Lcd_Init();
    Lcd_Cmd(_LCD_CLEAR);
    Lcd_Cmd(_LCD_CURSOR_OFF);
    //lcd_out(1,1,"Trabalho RTC");
    //delay_ms(1000);
    lcd_cmd(_lcd_clear);
    lcd_out(1,3,"/");
    lcd_out(1,7,"/");
    lcd_out(1,8,"20");

    rtc_init();

    adc1_init();
    adc_set_input_channel(_adc_channel_1);

    current_duty = 1000;
    pwm_period = PWM_TIM1_Init(5000);
    PWM_TIM1_Set_Duty(current_duty, _PWM_NON_INVERTED, _PWM_CHANNEL1);
    PWM_TIM1_Start(_PWM_CHANNEL1, &_GPIO_MODULE_TIM1_CH1_PE9);

    while(1){
        get_data();
        get_hour();
        ldr();
        pwm();

        if(gpioe_idr.b2 == 0){
            while(gpioe_idr.b2 == 0)delay_ms(100);
            set_year_rtc();
        }

        if(gpioe_idr.b3 == 0){
            while(gpioe_idr.b3 == 0)delay_ms(100);
            set_month_rtc();
        }

        if(gpioe_idr.b4 == 0){
            while(gpioe_idr.b4 == 0)delay_ms(100);
            set_day_rtc();
        }

        if(gpioe_idr.b5 == 0){
            while(gpioe_idr.b5 == 0)delay_ms(100);
            set_hour_rtc();
        }

        if(gpioe_idr.b6 == 0){
            while(gpioe_idr.b6 == 0)delay_ms(100);
            set_minute_rtc();
        }
    }
}

void pwm(){
    current_duty = 1500 - (result * 1.6);
    if(current_duty > pwm_period){
        result = 0;
    }
    delay_ms(1);
    PWM_TIM1_Set_Duty(current_duty, _PWM_NON_INVERTED, _PWM_CHANNEL1);
}

void ldr(){
    result = adc1_read(1); //bit PA1

    aux_ldr = result + aux_ldr;
    cont_ldr++;

    if (cont_ldr > 200)
    {
        result = aux_ldr/cont_ldr;
        cont_ldr = 0;
        aux_ldr;
    }

    if(result < 900){
        lcd_out(1,16, "*");
    }else{
        lcd_out(1,16, " ");
    }

    intToStr(result, result_txt);
    lcd_out(2,11, result_txt);
}

void rtc_init(){
    unsigned short   RTC_Wait_ctr = 0;
    PWREN_bit = 1;
    DBP_bit = 1;
    RTCSEL1_bit = 0;
    RTCSEL0_bit = 1;
    LSEBYP_bit = 0;
    LSEON_bit = 1;
    RTCEN_bit = 1;
    DBP_bit = 1;
    RTC_WPR = 0xCA;
    RTC_WPR = 0x53;
    while ((LSERDY_bit == 0) && (RTC_Wait_ctr < 150));
    {
         delay_us(1);
         RTC_Wait_ctr++;
    }
    RTC_Wait_ctr = 0;
    if (INITF_bit == 0){
         RTC_ISR.B7 = 1;
         while ((INITF_bit == 0) && (RTC_Wait_ctr < 8)){
             delay_us(10);
             RTC_Wait_ctr++;
         }
         if (INITF_bit == 0){
             RTC_ISR.B7 = 0;
             RTC_WPR = 0xFF;
         }
    }
    RTC_PRERbits.PREDIV_S = 255;
    RTC_PRERbits.PREDIV_A = 127;
    FMT_bit = 0;//HR_Format;
    RTC_ISR.B7 = 0;
    RTC_WPR = 0xFF;
}

void get_hour(){
    aux = rtc_trbits.st;
    aux = aux << 4;
    aux = aux | rtc_trbits.su;
    second = bcd2dec(aux);
    aux = rtc_trbits.mnt;
    aux = aux << 4;
    aux = aux | rtc_trbits.mnu;
    minute = bcd2dec(aux);
    aux = rtc_trbits.ht;
    aux = aux << 4;
    aux = aux | rtc_trbits.hu;
    hour = bcd2dec(aux);
    lcd_out(2,1,"");
    bytetostr(hour,txt);
    if(hour < 10) lcd_chr_cp('0');
    lcd_out_cp(ltrim(txt));
    lcd_chr_cp(':');
    bytetostr(minute,txt);
    if(minute < 10) lcd_chr_cp('0');
    lcd_out_cp(ltrim(txt));
    lcd_chr_cp(':');
    if(second < 10) lcd_chr_cp('0');
    bytetostr(second,txt);
    lcd_out_cp(ltrim(txt));
}

void show_month(){
    if(month == 1)
    {
        lcd_out(1,4,"JAN");
    }
    if(month == 2)
    {
        lcd_out(1,4,"FEV");
    }
    if(month == 3)
    {
        lcd_out(1,4,"MAR");
    }
    if(month == 4)
    {
        lcd_out(1,4,"ABR");
    }
    if(month == 5)
    {
        lcd_out(1,4,"MAI");
    }
    if(month == 6)
    {
        lcd_out(1,4,"JUN");
    }
    if(month == 7)
    {
        lcd_out(1,4,"JUL");
    }
    if(month == 8)
    {
        lcd_out(1,4,"AGO");
    }
    if(month == 9)
    {
        lcd_out(1,4,"SET");
    }
    if(month == 10)
    {
        lcd_out(1,4,"OUT");
    }
    if(month == 11)
    {
        lcd_out(1,4,"NOV");
    }
    if(month == 12)
    {
        lcd_out(1,4,"DEZ");
    }
}

void get_data(){
    aux = RTC_DRBits.DT;
    aux = aux << 4;
    aux = aux | RTC_DRBits.du;
    day = bcd2dec(aux);
    aux = rtc_DRBits.mt;
    aux = aux << 4;
    aux = aux | rtc_DRBits.mu;
    month = bcd2dec(aux);
    aux = rtc_drbits.yt;
    aux = aux << 4;
    aux = aux | rtc_drbits.yu;
    year = bcd2dec(aux);

    bytetostr(day,txt);
    if(day < 10)
    {
        lcd_out(1,1,"0");
        lcd_out(1,2,ltrim(txt));
    }else
    {
        lcd_out(1,1,ltrim(txt));
    }

    show_month();

    bytetostr(year,txt);
    if(year < 10)
    {
        lcd_out(1,10,"0");
        lcd_out(1,11,ltrim(txt));
    }
    else
    {
        lcd_out(1,10,ltrim(txt));
    }
}

void set_rtc(){
    unsigned short   RTC_Wait_ctr = 0;
    DBP_bit = 1;
    RTC_WPR = 0xCA;
    RTC_WPR = 0x53;
    while ((LSERDY_bit == 0) && (RTC_Wait_ctr < 150));
    {
         delay_us(1);
         RTC_Wait_ctr++;
    }
    RTC_Wait_ctr = 0;
    if (INITF_bit == 0)
    {
        RTC_ISR.B7 = 1;
        while ((INITF_bit == 0) && (RTC_Wait_ctr < 8)){
            delay_us(10);
            RTC_Wait_ctr++;
        }
        if (INITF_bit == 0){
            RTC_ISR.B7 = 0;
            RTC_WPR = 0xFF;
        }

    aux = dec2bcd(hour);
    aux = aux << 8;
    aux = aux | dec2bcd(minute);
    aux = aux << 8;
    aux = aux | dec2bcd(second);
    rtc_tr = aux;
    aux = dec2bcd(year);
    aux = aux << 8;
    aux = aux | dec2bcd(month);
    aux = aux << 8;
    aux = aux | dec2bcd(day);
    rtc_dr = aux;

    RTC_ISR.B7 = 0;
    RTC_WPR = 0xFF;
}
}

void month_day(){
    if(month == 1)
    {
        day_month = 31;
    }

    if(month == 2)
    {
        if(year % 4 == 0)
        {
           day_month = 29;
        }
        else
          day_month = 28;
    }
    if(month == 3)
    {
        day_month = 31;
    }
    if(month == 4)
    {
        day_month = 310;
    }
    if(month == 5)
    {
        day_month = 31;
    }
    if(month == 6)
    {
        day_month = 30;
    }
    if(month == 7)
    {
        day_month = 31;
    }
    if(month == 8)
    {
        day_month = 31;
    }
    if(month == 9)
    {
        day_month = 30;
    }
    if(month == 10)
    {
        day_month = 31;
    }
    if(month == 11)
    {
        day_month = 30;
    }
    if(month == 12)
    {
        day_month = 31;
    }
}

void set_year_rtc(){
    if(year < 99){
        year += 1;
    }else{
        year = 0;
    }
    set_rtc();
}

void set_month_rtc(){
    if(month < 12){
        month += 1;
    }else{
        month = 1;
    }
    set_rtc();
}

void set_day_rtc(){
    month_day();
    if(day != day_month){
        day += 1;
    }else{
        day = 1;
    }
    set_rtc();
}

void set_hour_rtc(){
    if(hour < 23){
        hour += 1;
    }else{
        hour = 0;
    }
    set_rtc();
}

void set_minute_rtc(){
    if(minute < 60){
        minute += 1;
    }else{
        minute = 0;
    }
    second = 0;
    set_rtc();
}
