_main:
;final.c,40 :: 		void main(){
SUB	SP, SP, #4
;final.c,42 :: 		_gpio_cfg_mode_input | _gpio_cfg_pull_up);
MOVS	R2, #130
;final.c,41 :: 		gpio_config(&gpioe_base, _gpio_pinmask_2 | _gpio_pinmask_3 | _gpio_pinmask_4 | _gpio_pinmask_5 | _gpio_pinmask_6,
MOVS	R1, #124
MOVW	R0, #lo_addr(GPIOE_BASE+0)
MOVT	R0, #hi_addr(GPIOE_BASE+0)
;final.c,42 :: 		_gpio_cfg_mode_input | _gpio_cfg_pull_up);
BL	_GPIO_Config+0
;final.c,43 :: 		Lcd_Init();
BL	_Lcd_Init+0
;final.c,44 :: 		Lcd_Cmd(_LCD_CLEAR);
MOVS	R0, #1
BL	_Lcd_Cmd+0
;final.c,45 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);
MOVS	R0, #12
BL	_Lcd_Cmd+0
;final.c,48 :: 		lcd_cmd(_lcd_clear);
MOVS	R0, #1
BL	_Lcd_Cmd+0
;final.c,49 :: 		lcd_out(1,3,"/");
MOVW	R0, #lo_addr(?lstr1_final+0)
MOVT	R0, #hi_addr(?lstr1_final+0)
MOV	R2, R0
MOVS	R1, #3
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,50 :: 		lcd_out(1,7,"/");
MOVW	R0, #lo_addr(?lstr2_final+0)
MOVT	R0, #hi_addr(?lstr2_final+0)
MOV	R2, R0
MOVS	R1, #7
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,51 :: 		lcd_out(1,8,"20");
MOVW	R0, #lo_addr(?lstr3_final+0)
MOVT	R0, #hi_addr(?lstr3_final+0)
MOV	R2, R0
MOVS	R1, #8
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,53 :: 		rtc_init();
BL	_rtc_init+0
;final.c,55 :: 		adc1_init();
BL	_ADC1_Init+0
;final.c,56 :: 		adc_set_input_channel(_adc_channel_1);
MOVW	R0, #2
BL	_ADC_Set_Input_Channel+0
;final.c,58 :: 		current_duty = 1000;
MOVW	R1, #1000
MOVW	R0, #lo_addr(_current_duty+0)
MOVT	R0, #hi_addr(_current_duty+0)
STR	R0, [SP, #0]
STRH	R1, [R0, #0]
;final.c,59 :: 		pwm_period = PWM_TIM1_Init(5000);
MOVW	R0, #5000
BL	_PWM_TIM1_Init+0
MOVW	R1, #lo_addr(_pwm_period+0)
MOVT	R1, #hi_addr(_pwm_period+0)
STRH	R0, [R1, #0]
;final.c,60 :: 		PWM_TIM1_Set_Duty(current_duty, _PWM_NON_INVERTED, _PWM_CHANNEL1);
LDR	R0, [SP, #0]
LDRH	R0, [R0, #0]
MOVS	R2, #0
MOVS	R1, #0
BL	_PWM_TIM1_Set_Duty+0
;final.c,61 :: 		PWM_TIM1_Start(_PWM_CHANNEL1, &_GPIO_MODULE_TIM1_CH1_PE9);
MOVW	R1, #lo_addr(__GPIO_MODULE_TIM1_CH1_PE9+0)
MOVT	R1, #hi_addr(__GPIO_MODULE_TIM1_CH1_PE9+0)
MOVS	R0, #0
BL	_PWM_TIM1_Start+0
;final.c,63 :: 		while(1){
L_main0:
;final.c,64 :: 		get_data();
BL	_get_data+0
;final.c,65 :: 		get_hour();
BL	_get_hour+0
;final.c,66 :: 		ldr();
BL	_ldr+0
;final.c,67 :: 		pwm();
BL	_pwm+0
;final.c,69 :: 		if(gpioe_idr.b2 == 0){
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main2
;final.c,70 :: 		while(gpioe_idr.b2 == 0)delay_ms(100);
L_main3:
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main4
MOVW	R7, #46899
MOVT	R7, #12
NOP
NOP
L_main5:
SUBS	R7, R7, #1
BNE	L_main5
NOP
NOP
NOP
NOP
IT	AL
BAL	L_main3
L_main4:
;final.c,71 :: 		set_year_rtc();
BL	_set_year_rtc+0
;final.c,72 :: 		}
L_main2:
;final.c,74 :: 		if(gpioe_idr.b3 == 0){
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main7
;final.c,75 :: 		while(gpioe_idr.b3 == 0)delay_ms(100);
L_main8:
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main9
MOVW	R7, #46899
MOVT	R7, #12
NOP
NOP
L_main10:
SUBS	R7, R7, #1
BNE	L_main10
NOP
NOP
NOP
NOP
IT	AL
BAL	L_main8
L_main9:
;final.c,76 :: 		set_month_rtc();
BL	_set_month_rtc+0
;final.c,77 :: 		}
L_main7:
;final.c,79 :: 		if(gpioe_idr.b4 == 0){
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main12
;final.c,80 :: 		while(gpioe_idr.b4 == 0)delay_ms(100);
L_main13:
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main14
MOVW	R7, #46899
MOVT	R7, #12
NOP
NOP
L_main15:
SUBS	R7, R7, #1
BNE	L_main15
NOP
NOP
NOP
NOP
IT	AL
BAL	L_main13
L_main14:
;final.c,81 :: 		set_day_rtc();
BL	_set_day_rtc+0
;final.c,82 :: 		}
L_main12:
;final.c,84 :: 		if(gpioe_idr.b5 == 0){
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main17
;final.c,85 :: 		while(gpioe_idr.b5 == 0)delay_ms(100);
L_main18:
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main19
MOVW	R7, #46899
MOVT	R7, #12
NOP
NOP
L_main20:
SUBS	R7, R7, #1
BNE	L_main20
NOP
NOP
NOP
NOP
IT	AL
BAL	L_main18
L_main19:
;final.c,86 :: 		set_hour_rtc();
BL	_set_hour_rtc+0
;final.c,87 :: 		}
L_main17:
;final.c,89 :: 		if(gpioe_idr.b6 == 0){
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main22
;final.c,90 :: 		while(gpioe_idr.b6 == 0)delay_ms(100);
L_main23:
MOVW	R1, #lo_addr(GPIOE_IDR+0)
MOVT	R1, #hi_addr(GPIOE_IDR+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_main24
MOVW	R7, #46899
MOVT	R7, #12
NOP
NOP
L_main25:
SUBS	R7, R7, #1
BNE	L_main25
NOP
NOP
NOP
NOP
IT	AL
BAL	L_main23
L_main24:
;final.c,91 :: 		set_minute_rtc();
BL	_set_minute_rtc+0
;final.c,92 :: 		}
L_main22:
;final.c,93 :: 		}
IT	AL
BAL	L_main0
;final.c,94 :: 		}
L_end_main:
L__main_end_loop:
B	L__main_end_loop
; end of _main
_pwm:
;final.c,96 :: 		void pwm(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;final.c,97 :: 		current_duty = 1500 - (result * 1.6);
MOVW	R0, #lo_addr(_result+0)
MOVT	R0, #hi_addr(_result+0)
LDRSH	R0, [R0, #0]
VMOV	S1, R0
VCVT.F32	#1, S1, S1
MOVW	R0, #52429
MOVT	R0, #16332
VMOV	S0, R0
VMUL.F32	S1, S1, S0
MOVW	R0, #32768
MOVT	R0, #17595
VMOV	S0, R0
VSUB.F32	S0, S0, S1
VCVT	#1, .F32, S0, S0
VMOV	R1, S0
UXTH	R1, R1
MOVW	R0, #lo_addr(_current_duty+0)
MOVT	R0, #hi_addr(_current_duty+0)
STRH	R1, [R0, #0]
;final.c,98 :: 		if(current_duty > pwm_period){
MOVW	R0, #lo_addr(_pwm_period+0)
MOVT	R0, #hi_addr(_pwm_period+0)
LDRH	R0, [R0, #0]
CMP	R1, R0
IT	LS
BLS	L_pwm27
;final.c,99 :: 		result = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_result+0)
MOVT	R0, #hi_addr(_result+0)
STRH	R1, [R0, #0]
;final.c,100 :: 		}
L_pwm27:
;final.c,101 :: 		delay_ms(1);
MOVW	R7, #8331
MOVT	R7, #0
NOP
NOP
L_pwm28:
SUBS	R7, R7, #1
BNE	L_pwm28
NOP
NOP
NOP
NOP
;final.c,102 :: 		PWM_TIM1_Set_Duty(current_duty, _PWM_NON_INVERTED, _PWM_CHANNEL1);
MOVW	R0, #lo_addr(_current_duty+0)
MOVT	R0, #hi_addr(_current_duty+0)
LDRH	R0, [R0, #0]
MOVS	R2, #0
MOVS	R1, #0
BL	_PWM_TIM1_Set_Duty+0
;final.c,103 :: 		}
L_end_pwm:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _pwm
_ldr:
;final.c,105 :: 		void ldr(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;final.c,106 :: 		result = adc1_read(1); //bit PA1
MOVS	R0, #1
BL	_ADC1_Read+0
MOVW	R1, #lo_addr(_result+0)
MOVT	R1, #hi_addr(_result+0)
STRH	R0, [R1, #0]
;final.c,108 :: 		aux_ldr = result + aux_ldr;
MOVW	R2, #lo_addr(_aux_ldr+0)
MOVT	R2, #hi_addr(_aux_ldr+0)
LDRSH	R1, [R2, #0]
SXTH	R0, R0
ADDS	R0, R0, R1
STRH	R0, [R2, #0]
;final.c,109 :: 		cont_ldr++;
MOVW	R1, #lo_addr(_cont_ldr+0)
MOVT	R1, #hi_addr(_cont_ldr+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
SXTH	R0, R0
STRH	R0, [R1, #0]
;final.c,111 :: 		if (cont_ldr > 200)
CMP	R0, #200
IT	LE
BLE	L_ldr30
;final.c,113 :: 		result = aux_ldr/cont_ldr;
MOVW	R2, #lo_addr(_cont_ldr+0)
MOVT	R2, #hi_addr(_cont_ldr+0)
LDRSH	R1, [R2, #0]
MOVW	R0, #lo_addr(_aux_ldr+0)
MOVT	R0, #hi_addr(_aux_ldr+0)
LDRSH	R0, [R0, #0]
SDIV	R1, R0, R1
MOVW	R0, #lo_addr(_result+0)
MOVT	R0, #hi_addr(_result+0)
STRH	R1, [R0, #0]
;final.c,114 :: 		cont_ldr = 0;
MOVS	R0, #0
SXTH	R0, R0
STRH	R0, [R2, #0]
;final.c,116 :: 		}
L_ldr30:
;final.c,118 :: 		if(result < 900){
MOVW	R0, #lo_addr(_result+0)
MOVT	R0, #hi_addr(_result+0)
LDRSH	R0, [R0, #0]
CMP	R0, #900
IT	GE
BGE	L_ldr31
;final.c,119 :: 		lcd_out(1,16, "*");
MOVW	R0, #lo_addr(?lstr4_final+0)
MOVT	R0, #hi_addr(?lstr4_final+0)
MOV	R2, R0
MOVS	R1, #16
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,120 :: 		}else{
IT	AL
BAL	L_ldr32
L_ldr31:
;final.c,121 :: 		lcd_out(1,16, " ");
MOVW	R0, #lo_addr(?lstr5_final+0)
MOVT	R0, #hi_addr(?lstr5_final+0)
MOV	R2, R0
MOVS	R1, #16
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,122 :: 		}
L_ldr32:
;final.c,124 :: 		intToStr(result, result_txt);
MOVW	R0, #lo_addr(_result+0)
MOVT	R0, #hi_addr(_result+0)
LDRSH	R0, [R0, #0]
MOVW	R1, #lo_addr(_result_txt+0)
MOVT	R1, #hi_addr(_result_txt+0)
BL	_IntToStr+0
;final.c,125 :: 		lcd_out(2,11, result_txt);
MOVW	R2, #lo_addr(_result_txt+0)
MOVT	R2, #hi_addr(_result_txt+0)
MOVS	R1, #11
MOVS	R0, #2
BL	_Lcd_Out+0
;final.c,126 :: 		}
L_end_ldr:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _ldr
_rtc_init:
;final.c,128 :: 		void rtc_init(){
;final.c,129 :: 		unsigned short   RTC_Wait_ctr = 0;
; RTC_Wait_ctr start address is: 16 (R4)
MOVS	R4, #0
;final.c,130 :: 		PWREN_bit = 1;
MOVS	R3, #1
SXTB	R3, R3
MOVW	R0, #lo_addr(PWREN_bit+0)
MOVT	R0, #hi_addr(PWREN_bit+0)
STR	R3, [R0, #0]
;final.c,131 :: 		DBP_bit = 1;
MOVW	R2, #lo_addr(DBP_bit+0)
MOVT	R2, #hi_addr(DBP_bit+0)
STR	R3, [R2, #0]
;final.c,132 :: 		RTCSEL1_bit = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(RTCSEL1_bit+0)
MOVT	R0, #hi_addr(RTCSEL1_bit+0)
STR	R1, [R0, #0]
;final.c,133 :: 		RTCSEL0_bit = 1;
MOVW	R0, #lo_addr(RTCSEL0_bit+0)
MOVT	R0, #hi_addr(RTCSEL0_bit+0)
STR	R3, [R0, #0]
;final.c,134 :: 		LSEBYP_bit = 0;
MOVW	R0, #lo_addr(LSEBYP_bit+0)
MOVT	R0, #hi_addr(LSEBYP_bit+0)
STR	R1, [R0, #0]
;final.c,135 :: 		LSEON_bit = 1;
MOVW	R0, #lo_addr(LSEON_bit+0)
MOVT	R0, #hi_addr(LSEON_bit+0)
STR	R3, [R0, #0]
;final.c,136 :: 		RTCEN_bit = 1;
MOVW	R0, #lo_addr(RTCEN_bit+0)
MOVT	R0, #hi_addr(RTCEN_bit+0)
STR	R3, [R0, #0]
;final.c,137 :: 		DBP_bit = 1;
STR	R3, [R2, #0]
;final.c,138 :: 		RTC_WPR = 0xCA;
MOVS	R1, #202
MOVW	R0, #lo_addr(RTC_WPR+0)
MOVT	R0, #hi_addr(RTC_WPR+0)
STR	R1, [R0, #0]
;final.c,139 :: 		RTC_WPR = 0x53;
MOVS	R1, #83
MOVW	R0, #lo_addr(RTC_WPR+0)
MOVT	R0, #hi_addr(RTC_WPR+0)
STR	R1, [R0, #0]
; RTC_Wait_ctr end address is: 16 (R4)
UXTB	R2, R4
;final.c,140 :: 		while ((LSERDY_bit == 0) && (RTC_Wait_ctr < 150));
L_rtc_init33:
; RTC_Wait_ctr start address is: 8 (R2)
; RTC_Wait_ctr start address is: 8 (R2)
; RTC_Wait_ctr end address is: 8 (R2)
MOVW	R1, #lo_addr(LSERDY_bit+0)
MOVT	R1, #hi_addr(LSERDY_bit+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L__rtc_init107
; RTC_Wait_ctr end address is: 8 (R2)
; RTC_Wait_ctr start address is: 8 (R2)
CMP	R2, #150
IT	CS
BCS	L__rtc_init106
L__rtc_init105:
; RTC_Wait_ctr end address is: 8 (R2)
IT	AL
BAL	L_rtc_init33
L__rtc_init107:
L__rtc_init106:
;final.c,142 :: 		delay_us(1);
MOVW	R7, #6
MOVT	R7, #0
NOP
NOP
L_rtc_init37:
SUBS	R7, R7, #1
BNE	L_rtc_init37
NOP
NOP
NOP
NOP
;final.c,145 :: 		RTC_Wait_ctr = 0;
; RTC_Wait_ctr start address is: 8 (R2)
MOVS	R2, #0
;final.c,146 :: 		if (INITF_bit == 0){
MOVW	R1, #lo_addr(INITF_bit+0)
MOVT	R1, #hi_addr(INITF_bit+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_rtc_init39
;final.c,147 :: 		RTC_ISR.B7 = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(RTC_ISR+0)
MOVT	R0, #hi_addr(RTC_ISR+0)
STR	R1, [R0, #0]
; RTC_Wait_ctr end address is: 8 (R2)
;final.c,148 :: 		while ((INITF_bit == 0) && (RTC_Wait_ctr < 8)){
L_rtc_init40:
; RTC_Wait_ctr start address is: 8 (R2)
MOVW	R1, #lo_addr(INITF_bit+0)
MOVT	R1, #hi_addr(INITF_bit+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L__rtc_init109
CMP	R2, #8
IT	CS
BCS	L__rtc_init108
L__rtc_init104:
;final.c,149 :: 		delay_us(10);
MOVW	R7, #81
MOVT	R7, #0
NOP
NOP
L_rtc_init44:
SUBS	R7, R7, #1
BNE	L_rtc_init44
NOP
NOP
NOP
NOP
;final.c,150 :: 		RTC_Wait_ctr++;
ADDS	R2, R2, #1
UXTB	R2, R2
;final.c,151 :: 		}
; RTC_Wait_ctr end address is: 8 (R2)
IT	AL
BAL	L_rtc_init40
;final.c,148 :: 		while ((INITF_bit == 0) && (RTC_Wait_ctr < 8)){
L__rtc_init109:
L__rtc_init108:
;final.c,152 :: 		if (INITF_bit == 0){
MOVW	R1, #lo_addr(INITF_bit+0)
MOVT	R1, #hi_addr(INITF_bit+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_rtc_init46
;final.c,153 :: 		RTC_ISR.B7 = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(RTC_ISR+0)
MOVT	R0, #hi_addr(RTC_ISR+0)
STR	R1, [R0, #0]
;final.c,154 :: 		RTC_WPR = 0xFF;
MOVS	R1, #255
MOVW	R0, #lo_addr(RTC_WPR+0)
MOVT	R0, #hi_addr(RTC_WPR+0)
STR	R1, [R0, #0]
;final.c,155 :: 		}
L_rtc_init46:
;final.c,156 :: 		}
L_rtc_init39:
;final.c,157 :: 		RTC_PRERbits.PREDIV_S = 255;
MOVS	R2, #255
MOVW	R1, #lo_addr(RTC_PRERbits+0)
MOVT	R1, #hi_addr(RTC_PRERbits+0)
LDRH	R0, [R1, #0]
BFI	R0, R2, #0, #15
STRH	R0, [R1, #0]
;final.c,158 :: 		RTC_PRERbits.PREDIV_A = 127;
MOVS	R2, #127
MOVW	R1, #lo_addr(RTC_PRERbits+0)
MOVT	R1, #hi_addr(RTC_PRERbits+0)
LDR	R0, [R1, #0]
BFI	R0, R2, #16, #7
STR	R0, [R1, #0]
;final.c,159 :: 		FMT_bit = 0;//HR_Format;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(FMT_bit+0)
MOVT	R0, #hi_addr(FMT_bit+0)
STR	R1, [R0, #0]
;final.c,160 :: 		RTC_ISR.B7 = 0;
MOVW	R0, #lo_addr(RTC_ISR+0)
MOVT	R0, #hi_addr(RTC_ISR+0)
STR	R1, [R0, #0]
;final.c,161 :: 		RTC_WPR = 0xFF;
MOVS	R1, #255
MOVW	R0, #lo_addr(RTC_WPR+0)
MOVT	R0, #hi_addr(RTC_WPR+0)
STR	R1, [R0, #0]
;final.c,162 :: 		}
L_end_rtc_init:
BX	LR
; end of _rtc_init
_get_hour:
;final.c,164 :: 		void get_hour(){
SUB	SP, SP, #8
STR	LR, [SP, #0]
;final.c,165 :: 		aux = rtc_trbits.st;
MOVW	R0, #lo_addr(RTC_TRbits+0)
MOVT	R0, #hi_addr(RTC_TRbits+0)
LDRB	R0, [R0, #0]
UBFX	R0, R0, #4, #3
MOVW	R2, #lo_addr(_aux+0)
MOVT	R2, #hi_addr(_aux+0)
STR	R2, [SP, #4]
STR	R0, [R2, #0]
;final.c,166 :: 		aux = aux << 4;
MOV	R0, R2
LDR	R0, [R0, #0]
LSLS	R1, R0, #4
STR	R1, [R2, #0]
;final.c,167 :: 		aux = aux | rtc_trbits.su;
MOVW	R0, #lo_addr(RTC_TRbits+0)
MOVT	R0, #hi_addr(RTC_TRbits+0)
LDRB	R0, [R0, #0]
UBFX	R0, R0, #0, #4
ORR	R0, R1, R0, LSL #0
STR	R0, [R2, #0]
;final.c,168 :: 		second = bcd2dec(aux);
UXTB	R0, R0
BL	_Bcd2Dec+0
MOVW	R1, #lo_addr(_second+0)
MOVT	R1, #hi_addr(_second+0)
STRH	R0, [R1, #0]
;final.c,169 :: 		aux = rtc_trbits.mnt;
MOVW	R0, #lo_addr(RTC_TRbits+0)
MOVT	R0, #hi_addr(RTC_TRbits+0)
LDRH	R0, [R0, #0]
UBFX	R0, R0, #12, #3
LDR	R2, [SP, #4]
STR	R0, [R2, #0]
;final.c,170 :: 		aux = aux << 4;
MOV	R0, R2
LDR	R0, [R0, #0]
LSLS	R1, R0, #4
STR	R1, [R2, #0]
;final.c,171 :: 		aux = aux | rtc_trbits.mnu;
MOVW	R0, #lo_addr(RTC_TRbits+0)
MOVT	R0, #hi_addr(RTC_TRbits+0)
LDRH	R0, [R0, #0]
UBFX	R0, R0, #8, #4
ORR	R0, R1, R0, LSL #0
STR	R0, [R2, #0]
;final.c,172 :: 		minute = bcd2dec(aux);
UXTB	R0, R0
BL	_Bcd2Dec+0
MOVW	R1, #lo_addr(_minute+0)
MOVT	R1, #hi_addr(_minute+0)
STRH	R0, [R1, #0]
;final.c,173 :: 		aux = rtc_trbits.ht;
MOVW	R0, #lo_addr(RTC_TRbits+0)
MOVT	R0, #hi_addr(RTC_TRbits+0)
LDR	R0, [R0, #0]
UBFX	R0, R0, #20, #2
LDR	R2, [SP, #4]
STR	R0, [R2, #0]
;final.c,174 :: 		aux = aux << 4;
MOV	R0, R2
LDR	R0, [R0, #0]
LSLS	R1, R0, #4
STR	R1, [R2, #0]
;final.c,175 :: 		aux = aux | rtc_trbits.hu;
MOVW	R0, #lo_addr(RTC_TRbits+0)
MOVT	R0, #hi_addr(RTC_TRbits+0)
LDR	R0, [R0, #0]
UBFX	R0, R0, #16, #4
ORR	R0, R1, R0, LSL #0
STR	R0, [R2, #0]
;final.c,176 :: 		hour = bcd2dec(aux);
UXTB	R0, R0
BL	_Bcd2Dec+0
MOVW	R1, #lo_addr(_hour+0)
MOVT	R1, #hi_addr(_hour+0)
STRH	R0, [R1, #0]
;final.c,177 :: 		lcd_out(2,1,"");
MOVW	R0, #lo_addr(?lstr6_final+0)
MOVT	R0, #hi_addr(?lstr6_final+0)
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #2
BL	_Lcd_Out+0
;final.c,178 :: 		bytetostr(hour,txt);
MOVW	R0, #lo_addr(_hour+0)
MOVT	R0, #hi_addr(_hour+0)
LDRSH	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt+0)
MOVT	R1, #hi_addr(_txt+0)
BL	_ByteToStr+0
;final.c,179 :: 		if(hour < 10) lcd_chr_cp('0');
MOVW	R0, #lo_addr(_hour+0)
MOVT	R0, #hi_addr(_hour+0)
LDRSH	R0, [R0, #0]
CMP	R0, #10
IT	GE
BGE	L_get_hour47
MOVS	R0, #48
BL	_Lcd_Chr_CP+0
L_get_hour47:
;final.c,180 :: 		lcd_out_cp(ltrim(txt));
MOVW	R0, #lo_addr(_txt+0)
MOVT	R0, #hi_addr(_txt+0)
BL	_Ltrim+0
BL	_Lcd_Out_CP+0
;final.c,181 :: 		lcd_chr_cp(':');
MOVS	R0, #58
BL	_Lcd_Chr_CP+0
;final.c,182 :: 		bytetostr(minute,txt);
MOVW	R0, #lo_addr(_minute+0)
MOVT	R0, #hi_addr(_minute+0)
LDRSH	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt+0)
MOVT	R1, #hi_addr(_txt+0)
BL	_ByteToStr+0
;final.c,183 :: 		if(minute < 10) lcd_chr_cp('0');
MOVW	R0, #lo_addr(_minute+0)
MOVT	R0, #hi_addr(_minute+0)
LDRSH	R0, [R0, #0]
CMP	R0, #10
IT	GE
BGE	L_get_hour48
MOVS	R0, #48
BL	_Lcd_Chr_CP+0
L_get_hour48:
;final.c,184 :: 		lcd_out_cp(ltrim(txt));
MOVW	R0, #lo_addr(_txt+0)
MOVT	R0, #hi_addr(_txt+0)
BL	_Ltrim+0
BL	_Lcd_Out_CP+0
;final.c,185 :: 		lcd_chr_cp(':');
MOVS	R0, #58
BL	_Lcd_Chr_CP+0
;final.c,186 :: 		if(second < 10) lcd_chr_cp('0');
MOVW	R0, #lo_addr(_second+0)
MOVT	R0, #hi_addr(_second+0)
LDRSH	R0, [R0, #0]
CMP	R0, #10
IT	GE
BGE	L_get_hour49
MOVS	R0, #48
BL	_Lcd_Chr_CP+0
L_get_hour49:
;final.c,187 :: 		bytetostr(second,txt);
MOVW	R0, #lo_addr(_second+0)
MOVT	R0, #hi_addr(_second+0)
LDRSH	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt+0)
MOVT	R1, #hi_addr(_txt+0)
BL	_ByteToStr+0
;final.c,188 :: 		lcd_out_cp(ltrim(txt));
MOVW	R0, #lo_addr(_txt+0)
MOVT	R0, #hi_addr(_txt+0)
BL	_Ltrim+0
BL	_Lcd_Out_CP+0
;final.c,189 :: 		}
L_end_get_hour:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _get_hour
_show_month:
;final.c,191 :: 		void show_month(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;final.c,192 :: 		if(month == 1)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #1
IT	NE
BNE	L_show_month50
;final.c,194 :: 		lcd_out(1,4,"JAN");
MOVW	R0, #lo_addr(?lstr7_final+0)
MOVT	R0, #hi_addr(?lstr7_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,195 :: 		}
L_show_month50:
;final.c,196 :: 		if(month == 2)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #2
IT	NE
BNE	L_show_month51
;final.c,198 :: 		lcd_out(1,4,"FEV");
MOVW	R0, #lo_addr(?lstr8_final+0)
MOVT	R0, #hi_addr(?lstr8_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,199 :: 		}
L_show_month51:
;final.c,200 :: 		if(month == 3)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #3
IT	NE
BNE	L_show_month52
;final.c,202 :: 		lcd_out(1,4,"MAR");
MOVW	R0, #lo_addr(?lstr9_final+0)
MOVT	R0, #hi_addr(?lstr9_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,203 :: 		}
L_show_month52:
;final.c,204 :: 		if(month == 4)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #4
IT	NE
BNE	L_show_month53
;final.c,206 :: 		lcd_out(1,4,"ABR");
MOVW	R0, #lo_addr(?lstr10_final+0)
MOVT	R0, #hi_addr(?lstr10_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,207 :: 		}
L_show_month53:
;final.c,208 :: 		if(month == 5)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #5
IT	NE
BNE	L_show_month54
;final.c,210 :: 		lcd_out(1,4,"MAI");
MOVW	R0, #lo_addr(?lstr11_final+0)
MOVT	R0, #hi_addr(?lstr11_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,211 :: 		}
L_show_month54:
;final.c,212 :: 		if(month == 6)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #6
IT	NE
BNE	L_show_month55
;final.c,214 :: 		lcd_out(1,4,"JUN");
MOVW	R0, #lo_addr(?lstr12_final+0)
MOVT	R0, #hi_addr(?lstr12_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,215 :: 		}
L_show_month55:
;final.c,216 :: 		if(month == 7)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #7
IT	NE
BNE	L_show_month56
;final.c,218 :: 		lcd_out(1,4,"JUL");
MOVW	R0, #lo_addr(?lstr13_final+0)
MOVT	R0, #hi_addr(?lstr13_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,219 :: 		}
L_show_month56:
;final.c,220 :: 		if(month == 8)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #8
IT	NE
BNE	L_show_month57
;final.c,222 :: 		lcd_out(1,4,"AGO");
MOVW	R0, #lo_addr(?lstr14_final+0)
MOVT	R0, #hi_addr(?lstr14_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,223 :: 		}
L_show_month57:
;final.c,224 :: 		if(month == 9)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #9
IT	NE
BNE	L_show_month58
;final.c,226 :: 		lcd_out(1,4,"SET");
MOVW	R0, #lo_addr(?lstr15_final+0)
MOVT	R0, #hi_addr(?lstr15_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,227 :: 		}
L_show_month58:
;final.c,228 :: 		if(month == 10)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #10
IT	NE
BNE	L_show_month59
;final.c,230 :: 		lcd_out(1,4,"OUT");
MOVW	R0, #lo_addr(?lstr16_final+0)
MOVT	R0, #hi_addr(?lstr16_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,231 :: 		}
L_show_month59:
;final.c,232 :: 		if(month == 11)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #11
IT	NE
BNE	L_show_month60
;final.c,234 :: 		lcd_out(1,4,"NOV");
MOVW	R0, #lo_addr(?lstr17_final+0)
MOVT	R0, #hi_addr(?lstr17_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,235 :: 		}
L_show_month60:
;final.c,236 :: 		if(month == 12)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #12
IT	NE
BNE	L_show_month61
;final.c,238 :: 		lcd_out(1,4,"DEZ");
MOVW	R0, #lo_addr(?lstr18_final+0)
MOVT	R0, #hi_addr(?lstr18_final+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,239 :: 		}
L_show_month61:
;final.c,240 :: 		}
L_end_show_month:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _show_month
_get_data:
;final.c,242 :: 		void get_data(){
SUB	SP, SP, #12
STR	LR, [SP, #0]
;final.c,243 :: 		aux = RTC_DRBits.DT;
MOVW	R0, #lo_addr(RTC_DRbits+0)
MOVT	R0, #hi_addr(RTC_DRbits+0)
LDRB	R0, [R0, #0]
UBFX	R0, R0, #4, #2
MOVW	R2, #lo_addr(_aux+0)
MOVT	R2, #hi_addr(_aux+0)
STR	R2, [SP, #8]
STR	R0, [R2, #0]
;final.c,244 :: 		aux = aux << 4;
MOV	R0, R2
LDR	R0, [R0, #0]
LSLS	R1, R0, #4
STR	R1, [R2, #0]
;final.c,245 :: 		aux = aux | RTC_DRBits.du;
MOVW	R0, #lo_addr(RTC_DRbits+0)
MOVT	R0, #hi_addr(RTC_DRbits+0)
LDRB	R0, [R0, #0]
UBFX	R0, R0, #0, #4
ORR	R0, R1, R0, LSL #0
STR	R0, [R2, #0]
;final.c,246 :: 		day = bcd2dec(aux);
UXTB	R0, R0
BL	_Bcd2Dec+0
MOVW	R1, #lo_addr(_day+0)
MOVT	R1, #hi_addr(_day+0)
STR	R1, [SP, #4]
STRH	R0, [R1, #0]
;final.c,247 :: 		aux = rtc_DRBits.mt;
MOVW	R1, #lo_addr(RTC_DRbits+0)
MOVT	R1, #hi_addr(RTC_DRbits+0)
LDR	R0, [R1, #0]
LDR	R2, [SP, #8]
STR	R0, [R2, #0]
;final.c,248 :: 		aux = aux << 4;
MOV	R0, R2
LDR	R0, [R0, #0]
LSLS	R1, R0, #4
STR	R1, [R2, #0]
;final.c,249 :: 		aux = aux | rtc_DRBits.mu;
MOVW	R0, #lo_addr(RTC_DRbits+0)
MOVT	R0, #hi_addr(RTC_DRbits+0)
LDRH	R0, [R0, #0]
UBFX	R0, R0, #8, #4
ORR	R0, R1, R0, LSL #0
STR	R0, [R2, #0]
;final.c,250 :: 		month = bcd2dec(aux);
UXTB	R0, R0
BL	_Bcd2Dec+0
MOVW	R1, #lo_addr(_month+0)
MOVT	R1, #hi_addr(_month+0)
STRH	R0, [R1, #0]
;final.c,251 :: 		aux = rtc_drbits.yt;
MOVW	R0, #lo_addr(RTC_DRbits+0)
MOVT	R0, #hi_addr(RTC_DRbits+0)
LDR	R0, [R0, #0]
UBFX	R0, R0, #20, #4
LDR	R2, [SP, #8]
STR	R0, [R2, #0]
;final.c,252 :: 		aux = aux << 4;
MOV	R0, R2
LDR	R0, [R0, #0]
LSLS	R1, R0, #4
STR	R1, [R2, #0]
;final.c,253 :: 		aux = aux | rtc_drbits.yu;
MOVW	R0, #lo_addr(RTC_DRbits+0)
MOVT	R0, #hi_addr(RTC_DRbits+0)
LDR	R0, [R0, #0]
UBFX	R0, R0, #16, #4
ORR	R0, R1, R0, LSL #0
STR	R0, [R2, #0]
;final.c,254 :: 		year = bcd2dec(aux);
UXTB	R0, R0
BL	_Bcd2Dec+0
MOVW	R1, #lo_addr(_year+0)
MOVT	R1, #hi_addr(_year+0)
STRH	R0, [R1, #0]
;final.c,256 :: 		bytetostr(day,txt);
LDR	R0, [SP, #4]
LDRSH	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt+0)
MOVT	R1, #hi_addr(_txt+0)
BL	_ByteToStr+0
;final.c,257 :: 		if(day < 10)
MOVW	R0, #lo_addr(_day+0)
MOVT	R0, #hi_addr(_day+0)
LDRSH	R0, [R0, #0]
CMP	R0, #10
IT	GE
BGE	L_get_data62
;final.c,259 :: 		lcd_out(1,1,"0");
MOVW	R0, #lo_addr(?lstr19_final+0)
MOVT	R0, #hi_addr(?lstr19_final+0)
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,260 :: 		lcd_out(1,2,ltrim(txt));
MOVW	R0, #lo_addr(_txt+0)
MOVT	R0, #hi_addr(_txt+0)
BL	_Ltrim+0
MOV	R2, R0
MOVS	R1, #2
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,261 :: 		}else
IT	AL
BAL	L_get_data63
L_get_data62:
;final.c,263 :: 		lcd_out(1,1,ltrim(txt));
MOVW	R0, #lo_addr(_txt+0)
MOVT	R0, #hi_addr(_txt+0)
BL	_Ltrim+0
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,264 :: 		}
L_get_data63:
;final.c,266 :: 		show_month();
BL	_show_month+0
;final.c,268 :: 		bytetostr(year,txt);
MOVW	R0, #lo_addr(_year+0)
MOVT	R0, #hi_addr(_year+0)
LDRSH	R0, [R0, #0]
MOVW	R1, #lo_addr(_txt+0)
MOVT	R1, #hi_addr(_txt+0)
BL	_ByteToStr+0
;final.c,269 :: 		if(year < 10)
MOVW	R0, #lo_addr(_year+0)
MOVT	R0, #hi_addr(_year+0)
LDRSH	R0, [R0, #0]
CMP	R0, #10
IT	GE
BGE	L_get_data64
;final.c,271 :: 		lcd_out(1,10,"0");
MOVW	R0, #lo_addr(?lstr20_final+0)
MOVT	R0, #hi_addr(?lstr20_final+0)
MOV	R2, R0
MOVS	R1, #10
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,272 :: 		lcd_out(1,11,ltrim(txt));
MOVW	R0, #lo_addr(_txt+0)
MOVT	R0, #hi_addr(_txt+0)
BL	_Ltrim+0
MOV	R2, R0
MOVS	R1, #11
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,273 :: 		}
IT	AL
BAL	L_get_data65
L_get_data64:
;final.c,276 :: 		lcd_out(1,10,ltrim(txt));
MOVW	R0, #lo_addr(_txt+0)
MOVT	R0, #hi_addr(_txt+0)
BL	_Ltrim+0
MOV	R2, R0
MOVS	R1, #10
MOVS	R0, #1
BL	_Lcd_Out+0
;final.c,277 :: 		}
L_get_data65:
;final.c,278 :: 		}
L_end_get_data:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _get_data
_set_rtc:
;final.c,280 :: 		void set_rtc(){
SUB	SP, SP, #8
STR	LR, [SP, #0]
;final.c,281 :: 		unsigned short   RTC_Wait_ctr = 0;
; RTC_Wait_ctr start address is: 8 (R2)
MOVS	R2, #0
;final.c,282 :: 		DBP_bit = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(DBP_bit+0)
MOVT	R0, #hi_addr(DBP_bit+0)
STR	R1, [R0, #0]
;final.c,283 :: 		RTC_WPR = 0xCA;
MOVS	R1, #202
MOVW	R0, #lo_addr(RTC_WPR+0)
MOVT	R0, #hi_addr(RTC_WPR+0)
STR	R1, [R0, #0]
;final.c,284 :: 		RTC_WPR = 0x53;
MOVS	R1, #83
MOVW	R0, #lo_addr(RTC_WPR+0)
MOVT	R0, #hi_addr(RTC_WPR+0)
STR	R1, [R0, #0]
;final.c,285 :: 		while ((LSERDY_bit == 0) && (RTC_Wait_ctr < 150));
L_set_rtc66:
; RTC_Wait_ctr start address is: 8 (R2)
; RTC_Wait_ctr end address is: 8 (R2)
MOVW	R1, #lo_addr(LSERDY_bit+0)
MOVT	R1, #hi_addr(LSERDY_bit+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L__set_rtc113
; RTC_Wait_ctr end address is: 8 (R2)
; RTC_Wait_ctr start address is: 8 (R2)
CMP	R2, #150
IT	CS
BCS	L__set_rtc112
L__set_rtc111:
; RTC_Wait_ctr end address is: 8 (R2)
IT	AL
BAL	L_set_rtc66
L__set_rtc113:
L__set_rtc112:
;final.c,287 :: 		delay_us(1);
MOVW	R7, #6
MOVT	R7, #0
NOP
NOP
L_set_rtc70:
SUBS	R7, R7, #1
BNE	L_set_rtc70
NOP
NOP
NOP
NOP
;final.c,290 :: 		RTC_Wait_ctr = 0;
; RTC_Wait_ctr start address is: 8 (R2)
MOVS	R2, #0
;final.c,291 :: 		if (INITF_bit == 0)
MOVW	R1, #lo_addr(INITF_bit+0)
MOVT	R1, #hi_addr(INITF_bit+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_set_rtc72
;final.c,293 :: 		RTC_ISR.B7 = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(RTC_ISR+0)
MOVT	R0, #hi_addr(RTC_ISR+0)
STR	R1, [R0, #0]
; RTC_Wait_ctr end address is: 8 (R2)
;final.c,294 :: 		while ((INITF_bit == 0) && (RTC_Wait_ctr < 8)){
L_set_rtc73:
; RTC_Wait_ctr start address is: 8 (R2)
MOVW	R1, #lo_addr(INITF_bit+0)
MOVT	R1, #hi_addr(INITF_bit+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L__set_rtc115
CMP	R2, #8
IT	CS
BCS	L__set_rtc114
L__set_rtc110:
;final.c,295 :: 		delay_us(10);
MOVW	R7, #81
MOVT	R7, #0
NOP
NOP
L_set_rtc77:
SUBS	R7, R7, #1
BNE	L_set_rtc77
NOP
NOP
NOP
NOP
;final.c,296 :: 		RTC_Wait_ctr++;
ADDS	R2, R2, #1
UXTB	R2, R2
;final.c,297 :: 		}
; RTC_Wait_ctr end address is: 8 (R2)
IT	AL
BAL	L_set_rtc73
;final.c,294 :: 		while ((INITF_bit == 0) && (RTC_Wait_ctr < 8)){
L__set_rtc115:
L__set_rtc114:
;final.c,298 :: 		if (INITF_bit == 0){
MOVW	R1, #lo_addr(INITF_bit+0)
MOVT	R1, #hi_addr(INITF_bit+0)
LDR	R0, [R1, #0]
CMP	R0, #0
IT	NE
BNE	L_set_rtc79
;final.c,299 :: 		RTC_ISR.B7 = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(RTC_ISR+0)
MOVT	R0, #hi_addr(RTC_ISR+0)
STR	R1, [R0, #0]
;final.c,300 :: 		RTC_WPR = 0xFF;
MOVS	R1, #255
MOVW	R0, #lo_addr(RTC_WPR+0)
MOVT	R0, #hi_addr(RTC_WPR+0)
STR	R1, [R0, #0]
;final.c,301 :: 		}
L_set_rtc79:
;final.c,303 :: 		aux = dec2bcd(hour);
MOVW	R0, #lo_addr(_hour+0)
MOVT	R0, #hi_addr(_hour+0)
LDRSH	R0, [R0, #0]
BL	_Dec2Bcd+0
MOVW	R1, #lo_addr(_aux+0)
MOVT	R1, #hi_addr(_aux+0)
STR	R1, [SP, #4]
STR	R0, [R1, #0]
;final.c,304 :: 		aux = aux << 8;
MOV	R0, R1
LDR	R0, [R0, #0]
LSLS	R0, R0, #8
STR	R0, [R1, #0]
;final.c,305 :: 		aux = aux | dec2bcd(minute);
MOVW	R0, #lo_addr(_minute+0)
MOVT	R0, #hi_addr(_minute+0)
LDRSH	R0, [R0, #0]
BL	_Dec2Bcd+0
MOVW	R2, #lo_addr(_aux+0)
MOVT	R2, #hi_addr(_aux+0)
LDR	R1, [R2, #0]
ORR	R0, R1, R0, LSL #0
STR	R0, [R2, #0]
;final.c,306 :: 		aux = aux << 8;
LSLS	R1, R0, #8
LDR	R0, [SP, #4]
STR	R1, [R0, #0]
;final.c,307 :: 		aux = aux | dec2bcd(second);
MOVW	R0, #lo_addr(_second+0)
MOVT	R0, #hi_addr(_second+0)
LDRSH	R0, [R0, #0]
BL	_Dec2Bcd+0
MOVW	R2, #lo_addr(_aux+0)
MOVT	R2, #hi_addr(_aux+0)
LDR	R1, [R2, #0]
ORRS	R1, R0
STR	R1, [R2, #0]
;final.c,308 :: 		rtc_tr = aux;
MOVW	R0, #lo_addr(RTC_TR+0)
MOVT	R0, #hi_addr(RTC_TR+0)
STR	R1, [R0, #0]
;final.c,309 :: 		aux = dec2bcd(year);
MOVW	R0, #lo_addr(_year+0)
MOVT	R0, #hi_addr(_year+0)
LDRSH	R0, [R0, #0]
BL	_Dec2Bcd+0
MOVW	R1, #lo_addr(_aux+0)
MOVT	R1, #hi_addr(_aux+0)
STR	R0, [R1, #0]
;final.c,310 :: 		aux = aux << 8;
LDR	R1, [SP, #4]
MOV	R0, R1
LDR	R0, [R0, #0]
LSLS	R0, R0, #8
STR	R0, [R1, #0]
;final.c,311 :: 		aux = aux | dec2bcd(month);
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
BL	_Dec2Bcd+0
MOVW	R2, #lo_addr(_aux+0)
MOVT	R2, #hi_addr(_aux+0)
LDR	R1, [R2, #0]
ORR	R0, R1, R0, LSL #0
STR	R0, [R2, #0]
;final.c,312 :: 		aux = aux << 8;
LSLS	R1, R0, #8
LDR	R0, [SP, #4]
STR	R1, [R0, #0]
;final.c,313 :: 		aux = aux | dec2bcd(day);
MOVW	R0, #lo_addr(_day+0)
MOVT	R0, #hi_addr(_day+0)
LDRSH	R0, [R0, #0]
BL	_Dec2Bcd+0
MOVW	R2, #lo_addr(_aux+0)
MOVT	R2, #hi_addr(_aux+0)
LDR	R1, [R2, #0]
ORRS	R1, R0
STR	R1, [R2, #0]
;final.c,314 :: 		rtc_dr = aux;
MOVW	R0, #lo_addr(RTC_DR+0)
MOVT	R0, #hi_addr(RTC_DR+0)
STR	R1, [R0, #0]
;final.c,316 :: 		RTC_ISR.B7 = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(RTC_ISR+0)
MOVT	R0, #hi_addr(RTC_ISR+0)
STR	R1, [R0, #0]
;final.c,317 :: 		RTC_WPR = 0xFF;
MOVS	R1, #255
MOVW	R0, #lo_addr(RTC_WPR+0)
MOVT	R0, #hi_addr(RTC_WPR+0)
STR	R1, [R0, #0]
;final.c,318 :: 		}
L_set_rtc72:
;final.c,319 :: 		}
L_end_set_rtc:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _set_rtc
_month_day:
;final.c,321 :: 		void month_day(){
;final.c,322 :: 		if(month == 1)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #1
IT	NE
BNE	L_month_day80
;final.c,324 :: 		day_month = 31;
MOVS	R1, #31
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,325 :: 		}
L_month_day80:
;final.c,327 :: 		if(month == 2)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #2
IT	NE
BNE	L_month_day81
;final.c,329 :: 		if(year % 4 == 0)
MOVW	R0, #lo_addr(_year+0)
MOVT	R0, #hi_addr(_year+0)
LDRSH	R2, [R0, #0]
MOVS	R1, #4
SXTH	R1, R1
SDIV	R0, R2, R1
MLS	R0, R1, R0, R2
SXTH	R0, R0
CMP	R0, #0
IT	NE
BNE	L_month_day82
;final.c,331 :: 		day_month = 29;
MOVS	R1, #29
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,332 :: 		}
IT	AL
BAL	L_month_day83
L_month_day82:
;final.c,334 :: 		day_month = 28;
MOVS	R1, #28
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
L_month_day83:
;final.c,335 :: 		}
L_month_day81:
;final.c,336 :: 		if(month == 3)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #3
IT	NE
BNE	L_month_day84
;final.c,338 :: 		day_month = 31;
MOVS	R1, #31
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,339 :: 		}
L_month_day84:
;final.c,340 :: 		if(month == 4)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #4
IT	NE
BNE	L_month_day85
;final.c,342 :: 		day_month = 310;
MOVW	R1, #310
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,343 :: 		}
L_month_day85:
;final.c,344 :: 		if(month == 5)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #5
IT	NE
BNE	L_month_day86
;final.c,346 :: 		day_month = 31;
MOVS	R1, #31
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,347 :: 		}
L_month_day86:
;final.c,348 :: 		if(month == 6)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #6
IT	NE
BNE	L_month_day87
;final.c,350 :: 		day_month = 30;
MOVS	R1, #30
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,351 :: 		}
L_month_day87:
;final.c,352 :: 		if(month == 7)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #7
IT	NE
BNE	L_month_day88
;final.c,354 :: 		day_month = 31;
MOVS	R1, #31
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,355 :: 		}
L_month_day88:
;final.c,356 :: 		if(month == 8)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #8
IT	NE
BNE	L_month_day89
;final.c,358 :: 		day_month = 31;
MOVS	R1, #31
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,359 :: 		}
L_month_day89:
;final.c,360 :: 		if(month == 9)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #9
IT	NE
BNE	L_month_day90
;final.c,362 :: 		day_month = 30;
MOVS	R1, #30
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,363 :: 		}
L_month_day90:
;final.c,364 :: 		if(month == 10)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #10
IT	NE
BNE	L_month_day91
;final.c,366 :: 		day_month = 31;
MOVS	R1, #31
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,367 :: 		}
L_month_day91:
;final.c,368 :: 		if(month == 11)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #11
IT	NE
BNE	L_month_day92
;final.c,370 :: 		day_month = 30;
MOVS	R1, #30
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,371 :: 		}
L_month_day92:
;final.c,372 :: 		if(month == 12)
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #12
IT	NE
BNE	L_month_day93
;final.c,374 :: 		day_month = 31;
MOVS	R1, #31
SXTH	R1, R1
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
STRH	R1, [R0, #0]
;final.c,375 :: 		}
L_month_day93:
;final.c,376 :: 		}
L_end_month_day:
BX	LR
; end of _month_day
_set_year_rtc:
;final.c,378 :: 		void set_year_rtc(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;final.c,379 :: 		if(year < 99){
MOVW	R0, #lo_addr(_year+0)
MOVT	R0, #hi_addr(_year+0)
LDRSH	R0, [R0, #0]
CMP	R0, #99
IT	GE
BGE	L_set_year_rtc94
;final.c,380 :: 		year += 1;
MOVW	R1, #lo_addr(_year+0)
MOVT	R1, #hi_addr(_year+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;final.c,381 :: 		}else{
IT	AL
BAL	L_set_year_rtc95
L_set_year_rtc94:
;final.c,382 :: 		year = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_year+0)
MOVT	R0, #hi_addr(_year+0)
STRH	R1, [R0, #0]
;final.c,383 :: 		}
L_set_year_rtc95:
;final.c,384 :: 		set_rtc();
BL	_set_rtc+0
;final.c,385 :: 		}
L_end_set_year_rtc:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _set_year_rtc
_set_month_rtc:
;final.c,387 :: 		void set_month_rtc(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;final.c,388 :: 		if(month < 12){
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
LDRSH	R0, [R0, #0]
CMP	R0, #12
IT	GE
BGE	L_set_month_rtc96
;final.c,389 :: 		month += 1;
MOVW	R1, #lo_addr(_month+0)
MOVT	R1, #hi_addr(_month+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;final.c,390 :: 		}else{
IT	AL
BAL	L_set_month_rtc97
L_set_month_rtc96:
;final.c,391 :: 		month = 1;
MOVS	R1, #1
SXTH	R1, R1
MOVW	R0, #lo_addr(_month+0)
MOVT	R0, #hi_addr(_month+0)
STRH	R1, [R0, #0]
;final.c,392 :: 		}
L_set_month_rtc97:
;final.c,393 :: 		set_rtc();
BL	_set_rtc+0
;final.c,394 :: 		}
L_end_set_month_rtc:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _set_month_rtc
_set_day_rtc:
;final.c,396 :: 		void set_day_rtc(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;final.c,397 :: 		month_day();
BL	_month_day+0
;final.c,398 :: 		if(day != day_month){
MOVW	R0, #lo_addr(_day_month+0)
MOVT	R0, #hi_addr(_day_month+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(_day+0)
MOVT	R0, #hi_addr(_day+0)
LDRSH	R0, [R0, #0]
CMP	R0, R1
IT	EQ
BEQ	L_set_day_rtc98
;final.c,399 :: 		day += 1;
MOVW	R1, #lo_addr(_day+0)
MOVT	R1, #hi_addr(_day+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;final.c,400 :: 		}else{
IT	AL
BAL	L_set_day_rtc99
L_set_day_rtc98:
;final.c,401 :: 		day = 1;
MOVS	R1, #1
SXTH	R1, R1
MOVW	R0, #lo_addr(_day+0)
MOVT	R0, #hi_addr(_day+0)
STRH	R1, [R0, #0]
;final.c,402 :: 		}
L_set_day_rtc99:
;final.c,403 :: 		set_rtc();
BL	_set_rtc+0
;final.c,404 :: 		}
L_end_set_day_rtc:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _set_day_rtc
_set_hour_rtc:
;final.c,406 :: 		void set_hour_rtc(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;final.c,407 :: 		if(hour < 23){
MOVW	R0, #lo_addr(_hour+0)
MOVT	R0, #hi_addr(_hour+0)
LDRSH	R0, [R0, #0]
CMP	R0, #23
IT	GE
BGE	L_set_hour_rtc100
;final.c,408 :: 		hour += 1;
MOVW	R1, #lo_addr(_hour+0)
MOVT	R1, #hi_addr(_hour+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;final.c,409 :: 		}else{
IT	AL
BAL	L_set_hour_rtc101
L_set_hour_rtc100:
;final.c,410 :: 		hour = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_hour+0)
MOVT	R0, #hi_addr(_hour+0)
STRH	R1, [R0, #0]
;final.c,411 :: 		}
L_set_hour_rtc101:
;final.c,412 :: 		set_rtc();
BL	_set_rtc+0
;final.c,413 :: 		}
L_end_set_hour_rtc:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _set_hour_rtc
_set_minute_rtc:
;final.c,415 :: 		void set_minute_rtc(){
SUB	SP, SP, #4
STR	LR, [SP, #0]
;final.c,416 :: 		if(minute < 60){
MOVW	R0, #lo_addr(_minute+0)
MOVT	R0, #hi_addr(_minute+0)
LDRSH	R0, [R0, #0]
CMP	R0, #60
IT	GE
BGE	L_set_minute_rtc102
;final.c,417 :: 		minute += 1;
MOVW	R1, #lo_addr(_minute+0)
MOVT	R1, #hi_addr(_minute+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;final.c,418 :: 		}else{
IT	AL
BAL	L_set_minute_rtc103
L_set_minute_rtc102:
;final.c,419 :: 		minute = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_minute+0)
MOVT	R0, #hi_addr(_minute+0)
STRH	R1, [R0, #0]
;final.c,420 :: 		}
L_set_minute_rtc103:
;final.c,421 :: 		second = 0;
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(_second+0)
MOVT	R0, #hi_addr(_second+0)
STRH	R1, [R0, #0]
;final.c,422 :: 		set_rtc();
BL	_set_rtc+0
;final.c,423 :: 		}
L_end_set_minute_rtc:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _set_minute_rtc
